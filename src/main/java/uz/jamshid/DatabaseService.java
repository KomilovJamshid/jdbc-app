package uz.jamshid;

import java.sql.*;

public class DatabaseService {
    private String url = "jdbc:postgresql://localhost:5432/app-jdbc-example";
    private String dbUser = "postgres";
    private String dbPassword = "1234567890";


    public void saveUser(User user) {
        try {
            Connection connection = DriverManager.getConnection(url, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            String query = "insert into users(first_name, last_name, username, password) " +
                    "values('" + user.getFirstName() + "','" + user.getLastName() + "','" + user.getUsername() + "','" + user.getPassword() + "');";
            statement.execute(query);
            System.out.println("User added");
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getUsers() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            String query = "select * from users";
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String first_name = resultSet.getString("first_name");
                String last_name = resultSet.getString("last_name");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                User user = new User(id, first_name, last_name, username, password);
                System.out.println(user);
            }
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteUser(int userId) {
        try {
            Connection connection = DriverManager.getConnection(url, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            String query = "delete from users where id=" + userId;
            statement.execute(query);
            System.out.println("User deleted");
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void editUser(User user) {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            String query = "update users set ";
            if (!user.getFirstName().isEmpty()) {
                query += "first_name='" + user.getFirstName() + "',";
            }
            if (!user.getLastName().isEmpty()) {
                query += "last_name='" + user.getLastName() + "',";
            }
            if (!user.getUsername().isEmpty()) {
                query += "username='" + user.getUsername() + "',";
            }
            if (!user.getPassword().isEmpty()) {
                query += "password='" + user.getPassword() + "'";
            }
            if (!query.equals("update users set ")) {
                if (query.endsWith("',")) {
                    query = query.substring(0, query.length() - 1);
                }
                query += " where id=" + user.getId();
                statement.execute(query);
                System.out.println("User edited");

            }
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void saveUserPreparedStatement(User user) {
        try {
            Connection connection = DriverManager.getConnection(url, dbUser, dbPassword);
            String query = "insert into users(first_name, last_name, username, password) values(?,?,?,?);";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getUsername());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.executeUpdate();
            System.out.println("User from prepared statement");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getUsersPreparedStatement() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, dbUser, dbPassword);
            String query = "select * from users";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String first_name = resultSet.getString("first_name");
                String last_name = resultSet.getString("last_name");
                String username = resultSet.getString("username");
                String password = resultSet.getString("password");
                User user = new User(id, first_name, last_name, username, password);
                System.out.println(user);
            }
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
