package uz.jamshid;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DatabaseService databaseService = new DatabaseService();

        int i = -1;

        while (i != 0) {
            System.out.println("0 => Exit \t 1 => Add User \t 2 => Edit User \t 3 => Delete User \t 4 => List Users");
            i = scanner.nextInt();
            switch (i) {
                case 1:
                    String firstName = requestString("Enter first name: ");
                    String lastName = requestString("Enter last name: ");
                    String username = requestString("Enter username: ");
                    String password = requestString("Enter password: ");
                    User user = new User(firstName, lastName, username, password);
//                    databaseService.saveUser(user);
                    databaseService.saveUserPreparedStatement(user);
                    break;
                case 2:
                    System.out.println("Enter user's id: ");
                    int id = scanner.nextInt();
                    firstName = requestString("Enter new first name: ");
                    lastName = requestString("Enter new last name: ");
                    username = requestString("Enter new username: ");
                    password = requestString("Enter new password: ");
                    user = new User(id, firstName, lastName, username, password);
                    databaseService.editUser(user);
                    break;
                case 3:
                    System.out.println("Enter user's id: ");
                    id = scanner.nextInt();
                    databaseService.deleteUser(id);
                    break;
                case 4:
                    databaseService.getUsers();
//                    databaseService.getUsersPreparedStatement();
                    break;

            }
        }
    }

    private static String requestString(String text) {
        System.out.println(text);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
